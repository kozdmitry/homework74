const express = require('express');
const router = express.Router();

const fs = require('fs');

router.get('/', (req, res) => {
    const path = "./messages";

    fs.readdir(path, (err, files) => {
        try{
            const data = [];
            files.forEach(file => {
                const result = JSON.parse(fs.readFileSync(`./messages/${file}`))
                data.push(result);
            });
            const lastFiveMessages = data.slice((data.length - 5), data.length);
            res.send(lastFiveMessages);

        } catch (err) {
            console.log(err)
        }

    })
});

router.post('/', (req, res) => {
    const message = req.body;
    message.date = new Date().toISOString();
    fs.writeFileSync(`./messages/${message.date}.txt`, JSON.stringify(message));
    res.send({message: 'Message saved!'})
});

module.exports = router;